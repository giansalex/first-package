<?php declare(strict_types=1);

namespace YourMonorepo\FirstPackage;

final class FirstClass
{
    public $id;
    public $name;
}
